# Fit Coach

Use your Otto DIY to remind you to take breaks frequently and motivate for exercising while  working at home during the Covid-19 pandemic.

# Development Environment

Follow these instructions to setup your development environment:

1. Download and 'install' [Sloeber](http://eclipse.baeyens.it/installAdvice.shtml), which is a free, open source, Eclipse IDE and open Arduino IDE alternative.
2. Start Sloeber by executing _sloeber-ide_ and provide a location for the new workspace to contain your projects.
3. Sloeber attempts to fully download and configure Arduino packages at startup.
   - **Note:** If configuring Sloeber fails, please ignore it for now.
4. Clone the Git repository and import project _fitcoach_ from the cloned Git repository.
5. In _Windows->Preferences->Arduino->Platforms and Boards_ select _arduino->Arduino AVR Boards->1.8.3_ and click _Apply and close_. This will start downloading and configuring the required Arduino packages for the Arduino Nano of your Otto DIY.
   - Also install the ArduinoSTL library. In case of compilation errors with `error: 'nothrow_t' in namespace 'std' does not name a type`, fix compilation of failed modules by changing `#include <new>` to `#include "new"` (for details: [link](https://github.com/mike-matera/ArduinoSTL/issues/56)) 
6. Right-click on the _fitcoach_ project and select _Properties_. 
7. Choose _Arduino_ and select:
   - the (predefined) folder in _Platform Folder_
   - _Arduino Nano_ in the _Board_ dropdown list
   - The USB port to which the Otto is connected in the _Port_ dropdown list (e.g. _/dev/ttyUSB0_)
   - Finally, _ATMega328P (Old Bootloader)_ in the _Processor_ dropdown list 
8. Download the [Otto DIY Library for C++](https://github.com/OttoDIY/OttoDIYLib) and place it on a local folder on your filesystem.
9. In the _Project Explorer_ right-click on _fitcoach->libraries_ and select _new folder_. Click _advanced_ and choose _link to alternate location (Linked Folder)_. Using _Browse_, select the folder where the Otto DIY Library for C++ is located (unzipped) on your filesystem. Press _Finish_.
10. Now build the _fitcoach_ project by right-click on the project in the _Project Explorer_ and select _Build Project_.
   * If you get a build error on _OttoDIYLib-Master/fontconvert/fontconvert.c_, simply remove this file from the library.
11. (Optional) Extend your Sloeber installation with Yakindu Statechart Tools, allowing you to change the statechart model and re-generate the implementation.The minimum features to install are:
   * The YAKINDU Statechart Tools Editor and Simulation Engine
   * The YAKINDU Statechart Tools for C/C++ Development
12. (Optional) Open the _Fitcoach.ysc_ statechart model in the project's _models_ folder. Check if the model can be correctly viewed and editted.
13. (Optional) Open the _Fitcoach.sgen_ generator model for the statechart model located in the _models_ folder. Check if the generator model can also be viewed and editted as expected.
14. (Optional) Regenerate the code located in the _src-gen_ folder by right-clicking on the generator model and choosing _Generate Code Artifacts_ from the menu. Unless a different version of YAKINDU Statechart Tools is used, no changes are expected in the _src-gen_ folder.

 


