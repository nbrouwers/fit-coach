#pragma once

#include <Otto9Humanoid.h>
#include <deque>

#define NR_SAMPLES (10)
#define MAX_WORKER_DISTANCE (90) // in cm
#define VALID_RANGE (150) // in cm

class PresenceDetector {
public:
	PresenceDetector(Otto9Humanoid* otto_ptr) :
		m_otto_ptr(otto_ptr),
		m_present(false) {
		for (int i=0 ; i < NR_SAMPLES ; i++){
			m_samples.push_front(0);
		}
	};
	virtual ~PresenceDetector() {};
	bool isPresent() {
		int sample = m_otto_ptr->getDistance();
		if (isValidSample(sample)) {
			m_samples.pop_front();
			m_samples.push_back(sample);
			std::deque<int> samples_sorted(m_samples);
			std::sort(samples_sorted.begin(), samples_sorted.end());
//			Serial.print("Samples: [");
//			for(auto it = samples_sorted.begin(); it != samples_sorted.end(); it++) {
//				Serial.print(*it);
//				Serial.print(" ");
//			}
//			Serial.println("]");
			m_present = (samples_sorted.at(NR_SAMPLES / 2) < MAX_WORKER_DISTANCE);
		}
		return m_present;
	};

private:
	Otto9Humanoid* m_otto_ptr;
	std::deque<int> m_samples;
	bool m_present;
	bool isValidSample(int sample) {
		return ((sample > 0) && (sample < VALID_RANGE));
	};
};

