#include <string.h>
#include <Otto9Humanoid.h>

#include "src-gen/FitCoach.h"
#include "src/sc_timer.h"
#include "src/sc_timer_service.h"
#include "src/sc_tracing.h"
#include "src/presence_detector.hpp"

Otto9Humanoid Otto;
FitCoach fc;
PresenceDetector presenceDetector(&Otto);
boolean workerDetected = false;

#define PIN_YL 2 // left leg, servo[0]
#define PIN_YR 3 // right leg, servo[1]
#define PIN_RL 4 // left foot, servo[2]
#define PIN_RR 5 // right foot, servo[3]
#define PIN_LA 6 //servo[4]  Left arm
#define PIN_RA 7 //servo[5]  Right arm
#define PIN_Trigger 8 // ultrasound
#define PIN_Echo 9 // ultrasound
#define PIN_NoiseSensor A6
#define PIN_Buzzer  13 //buzzer
#define DIN_PIN A3
#define CS_PIN A2
#define CLK_PIN A1
#define LED_DIRECTION 2
#define PIN_TouchSensor A0

#define CYCLE_PERIOD (200)

sc::timer::TimerTask tasks[FitCoach::timeEventsCount];
sc::timer::TimerService timer_sct(tasks, FitCoach::timeEventsCount);

class FitCoachOperationCallBack: public FitCoach::OperationCallback {

public:

	void sayHello() {
		Otto.playGesture(OttoSuperHappy);
		Otto.writeText("HELLO", ScrollSpeed);
		Otto.putMouth(happyOpen);
	}

	void sayBye() {
		Otto.playGesture(OttoSad);
		Otto.writeText("BYE", ScrollSpeed);
	}

	void sayEnjoy() {
		Otto.playGesture(OttoHappy);
		Otto.writeText("ENJOY", ScrollSpeed);
		Otto.putMouth(smile);
	}

	void sayPauze() {
		Otto.playGesture(OttoConfused);
		Otto.writeText("PAUZE", ScrollSpeed);
		Otto.putMouth(interrogation);
	}

	void sayRelax() {
		Otto.playGesture(OttoSleeping);
		Otto.writeText("RELAX", ScrollSpeed);
		Otto.putMouth(tongueOut);
	}

	void saySnoozed() {
		Otto.writeText("SNOOZED", ScrollSpeed);
		Otto.putMouth(smallSurprise);
	}

private:
	const byte ScrollSpeed = 100;
};

class TraceObserverImpl: public sc::trace::TraceObserver<
		FitCoach::FitCoachStates> {
public:
	TraceObserverImpl() {
	}
	;
	virtual ~TraceObserverImpl() {
	}
	;
	void stateEntered(FitCoach::FitCoachStates state) {
		String msg("Entered state: " + State2String(state));
		Serial.println(msg);
	}
	;
	void stateExited(FitCoach::FitCoachStates state) {
		String msg("Exited state: " + State2String(state));
		Serial.println(msg);
	}
	;

private:
	String State2String(FitCoach::FitCoachStates state) {
		String result;
		switch (state) {
		case FitCoach::main_region_On:
			result += "On";
			break;
		case FitCoach::main_region_Off:
			result += "Off";
			break;
		case FitCoach::main_region_Pauzing:
			result += "Pauzing";
			break;
		case FitCoach::main_region_RequestingPauze:
			result += "RequestingPauze";
			break;
		case FitCoach::main_region_Working:
			result += "Working";
			break;
		default:
			result += "UnknownState";
		}
		return result;
	}
	;
};

void setup() {
	pinMode(PIN_TouchSensor, INPUT);
	fc.setOperationCallback(new FitCoachOperationCallBack());
	fc.setTimerService(&timer_sct);
	fc.setTraceObserver(new TraceObserverImpl());
	fc.setWorkerDetected(false);
	fc.enter();
	Otto.initMATRIX( DIN_PIN, CS_PIN, CLK_PIN, LED_DIRECTION);
	Serial.begin(9600);
	Otto.initHUMANOID(PIN_YL, PIN_YR, PIN_RL, PIN_RR, PIN_LA, PIN_RA, true,
	PIN_NoiseSensor, PIN_Buzzer, PIN_Trigger, PIN_Echo);
}

void loop() {
	delay(CYCLE_PERIOD);

	timer_sct.proceed(CYCLE_PERIOD);

	// Read sensors to trigger events
	if (digitalRead(PIN_TouchSensor)) {
		fc.raiseTouched();
	}
	if (presenceDetector.isPresent()) {
		Serial.println("Worker detected");
		fc.setWorkerDetected(true);
	}
	else {
		Serial.println("Worker not detected.");
		fc.setWorkerDetected(false);
	}

	fc.runCycle();
}
