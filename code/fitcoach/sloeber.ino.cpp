#ifdef __IN_ECLIPSE__
//This is a automatic generated file
//Please do not modify this file
//If you touch this file your change will be overwritten during the next build
//This file has been generated on 2021-02-20 22:35:07

#include "Arduino.h"
#include <string.h>
#include <Otto9Humanoid.h>
#include "src-gen/FitCoach.h"
#include "src/sc_timer.h"
#include "src/sc_timer_service.h"
#include "src/sc_tracing.h"
#include "src/presence_detector.hpp"

void setup() ;
void loop() ;


#include "Fitcoach.ino"

#endif
